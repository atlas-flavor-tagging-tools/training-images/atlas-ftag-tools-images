# atlas-ftag-tools images

This repository contains Docker images for the use of the
[`atlas-ftag-tools` package](https://github.com/umami-hep/atlas-ftag-tools).
The images are built in the [`atlas-ftag-tools` repository on GitHub](https://github.com/umami-hep/atlas-ftag-tools) 
and then pushed to the container registry of this repository.

You can start an interactive shell in a container with your current working directory 
mounted into the container by using one of the commands provided below.

On a machine with Docker installed:
```bash
docker run -it --rm -v $PWD:/atlas-ftag-tools_container -w /atlas-ftag-tools_container gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/atlas-ftag-tools-images/atlas-ftag-tools:latest bash
```
On a machine/cluster with singularity installed:
```bash
singularity shell --contain -B $PWD docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/atlas-ftag-tools-images/atlas-ftag-tools:latest
```

## Development image

For development, another image is provided which contains all dependencies of the project but no install of `atlas-ftag-tools`.
It is well suited for use in [VSCode Dev Container](https://code.visualstudio.com/docs/devcontainers/containers) environments.

You can launch it similar as the slim image with the installation of `atlas-ftag-docs`, replacing the tag `latest` with `latest-dev`.


## Tags

For use in production, there are tags in the gitlab registry for every release of the project.


**The images are automatically updated via GitHub and pushed to this [repository registry](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-images/atlas-ftag-tools-images/container_registry).**
